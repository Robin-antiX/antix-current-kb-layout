# antiX current keyboard layout

antiX control centre tool to display current keyboard layout.

## Installation:

Download installer packages [xkb-layout-state.deb](https://gitlab.com/Robin-antiX/xkb-layout-state/-/tree/main/debian-installer-packages?ref_type=heads) and antix-current-kb-layout.deb along with the checksum files.
Then check integrity:
```
$ shasum -c antix-current-kb-layout.deb.sha512.sum 
OK

$ shasum -c xkblayout-state.deb.sha512.sum
OK
```

Only if both integrity checks return an OK, proceed with installation, otherwise redownload the files, they have been corrupted on transport then.

```
sudo apt-get install ./xkb-layout-state.deb ./antix-current-kb-layout control-centre-antix
```

In case you want to have the very few lines showing up on error or missing dependencies translated, install antix-samba-manager additionally, it is not needed for core functionality.

## Usage:

Either click its entry from within antiX control centre, section system → current keyboard layout.

Or start it from console by keying in:
```
$ antiX-current-kb-layout
```

It will show you your current keyboard variant layout map, selected either at system startup, or selected from antiX system tray language flag or layout switching shortcut, or selected from within in control centre keyboard layout selection tool.
This might come handy when working with multiple foreign language layouts, lables not printed matchingly on keys of your physical keyboard.
Moreover it reveals the 2nd, 3rd and 4th layer assignments of your keyboard layout and variant in use, also not printed on the physical keys usually.

## Details:
For sheet display of the layout the gnome keyboard layout display applett is used, and for detection of current layout the xkbd-layout-state tool, packaged for antix and taken from [nonpop/xkblayout-state](https://github.com/nonpop/xkblayout-state) (GPL v.2), is used.
As always in antiX, you can do these simple tasks yourself on console as well. It's just a couple of keystrokes. Script takes care of prerequisites, and combines the commands to makes the result available per mousclick within GUI.

## Support
For help, questions, suggestions and bug reporting please write to [antiXlinux forums](https://www.antixforum.com).

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

-----------
Written 2024 by Robin.antiX for antiX community.
